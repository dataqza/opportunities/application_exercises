# CSV parser exercise

Create a website that converts simple user provided CSV text to JSON and an html table.

- All code must be in one `.html` file.
- You can use any(or no) css or javascript library/framework (except for the csv-to-json converter).
- The csv-to-json converter must be all your own code.
- The page must have:
  - A way for the user to upload a `.csv` file (into memory, no backend storage).
  - A way for the user type (or paste in) text (in the csv format) as an alternate to uploading a file.
  - A button to click that will convert the uploaded file or input text into a JSON string.
  - Display the output JSON on the screen. This displayed JSON must be selectable.
  - Display the JSON data as a table on the page.
- Please don't minify your code.

## Test files

The `/test_files` directory contains 3 pairs of input and output files.
For each input file the csv-to-json converter should generate JSON that matches the output file for that test.

- test_1: Simple text
- test_2: Text with numbers
- test_3: Commas inside the text

## The goal

- The aim of this exercise is to facilitate a conversation around how you approached the task, how you chose to implement your solution, challenges you might have found and so on.
- The goal is not to create the perfect csv-to-json converter, those already exist.
- This task is not meant to be onerous or time consuming. At an absolute maximum, please don't spend more than a day on this.
- Feel free to ask questions or clarify the scope.
