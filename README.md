# Application Exercises

A collection of exercise projects for potential applicants to help understand someones approach to software development

## How to start

Fork this repository, write your solution and then send us a link to your forked repository.

## Exercises

Currently there is only one exercise:

- csv_parser
